.PHONY: help

help: ## helper
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

.DEFAULT_GOAL := help

CONF ?= ./.env
VERSION = "1.0"

version: ## version
	@echo ${VERSION}
##

ifeq ($(shell test -e $(CONF) && echo -n yes),yes)

include $(CONF)
export $(shell sed 's/=.*//' $(CONF))

TKN_RUNNER=""

sshkey: ## Generete new ssh key for default user
	@./setup.sh --ssh

deploy: down build up

up: 
	@docker-compose up -d
	
build: 
	@docker-compose build

down: 
	@docker-compose down

gitlab: ## enter in node master
	@docker-compose exec gitlab /bin/bash

master: ## enter in node master 
	@docker-compose exec node_master /bin/bash

nodea: ## enter in node master
	@docker-compose exec node_a /bin/bash

nodeb: ## enter in node master
	@docker-compose exec node_b /bin/bash

register-runner:
	@docker exec -it gitlab-runner gitlab-runner register -n -r $(TKN_RUNNER) -u http://$(ALIAS_GITLAB) --executor docker --docker-image docker:latest --docker-network-mode serverws_gitlab --docker-volumes "/var/run/docker.sock:/var/run/docker.sock"

else
DEFAULT_USER=""
DEFAULT_GROUP=""

setup: #first install
	@chmod +x ./setup.sh
	@./setup.sh --install-utils --ssh -u $(DEFAULT_USER) -g $(DEFAULT_GROUP)

manual: ## manuale configurazione 
	@echo "read Readme.md"
	@echo "Missing .env file"
endif
##
