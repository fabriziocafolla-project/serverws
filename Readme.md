# Server Work Station

### Avvio

    #parametri non obbligatori, per default sono valorizzati "www-data"
    make setup DEFAULT_USER=username DEFAULT_GROUP=groupname

    make deploy

    add '127.0.0.1 gitlab.prod.local' in /etc/hosts

    make gitlab

        vi /etc/gitlab/gitlab.rb

            external_url "https://gitlab.prod.local"
            nginx['redirect_http_to_https'] = false
            alertmanager['enable'] = true

        #creazione ssl (nomi dei file uguale all'url)
        openssl genrsa -out /etc/gitlab/ssl/gitlab.prod.local.key 2048
        openssl req -new -key /etc/gitlab/ssl/gitlab.prod.local.key -out  /etc/gitlab/ssl/gitlab.prod.local.csr
        openssl x509 -in  /etc/gitlab/ssl/gitlab.prod.local.csr -out  /etc/gitlab/ssl/gitlab.prod.local.crt -req -signkey  /etc/gitlab/ssl/gitlab.prod.local.key -days 365
        openssl rsa -des3 -in  /etc/gitlab/ssl/gitlab.prod.loca.key -out /etc/gitlab/ssl/gitlab.prod.local.pem
        openssl rsa -in /etc/gitlab/ssl/gitlab.prod.local.pem -out  /etc/gitlab/ssl/gitlab.prod.local.key

### A cosa serve?

Questo progetto open source nasce per simulare un ambiente server composto da un nodo MASTER e due nodi WORKER, con in aggiunta un server GITLAB che serve per gesitere i repository e la CI/CD dei progetti. Attraverso docker verrano creati i container che fungeranno da server, i container avviati sono quattro: 

- Nodo MASTER che ha il compito di gestire i nodi worker, attraverso quello che volte, per default sarà possibile la connessione ssh verso gli altri container e è installato Ansible per poter automatizzare le vostre azioni.

- Nodi WORKER verranno avviati due container distinti, nodo A e nodo B

- Nodo GITLAB che fungerà da server contentente repository.

- Nodo GITLAB-RUNNER che fungerà da server runner per la ci/cd di gitlab.


## Dettagli

- Il setup creerà il file .env, la cartella 'data' dentro la cartella container e l'utente serverws. La cartella data ospiterà il volumi che saranno poi usati dai containers

- Il file Makefile permette di avviare l'intero sistema. Avrete a disposizione i comandi di 'build', 'up', 'down', 'deploy' (che esegue tutti e tre), 'master' (entra all'interno del container master), 'gitlab', 'nodoa' e 'nodob' 

- Per accedere alla dashboard Gitlab http://gitlab.local:8080/

- All'interno dei container la password dell'utente root è 'root'. La password dell'utente default è 'serverws'

- Il noda master può comunicare attraverso gli host nodea.private, nodeb.private, gitlab.local