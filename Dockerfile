#BUILDER
ARG TAG="18.04"

FROM ubuntu:${TAG} AS builder

ARG WORKDIR_USER
ARG WORKDIR_GROUP
ARG WORKDIR_PATH

RUN test -n "${WORKDIR_USER}" || (echo "[BUILD ARG] WORKDIR_USER not set" && false) 
RUN test -n "${WORKDIR_GROUP}" || (echo "[BUILD ARG] WORKDIR_GROUP not set" && false)
RUN test -n "${WORKDIR_PATH}" || (echo "[BUILD ARG] WORKDIR_PATH not set" && false)

RUN apt-get update && \
  apt-get upgrade -y && \
  apt-get install -y --no-install-recommends \
        vim \
        build-essential \
        acl \
        iproute2 \
        iputils-ping \
        network-manager \
        ufw \
        ssh \
        man \
    && apt-get -qy autoremove \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

COPY ./container/sbin/entrypoint.sh /usr/local/sbin/serverws/entrypoint.sh
COPY ./container/sbin/base.sh /usr/local/sbin/serverws/base.sh
COPY ./container/sbin/setup /usr/local/sbin/serverws/setup

RUN chmod +x -R /usr/local/sbin/serverws \
    && /usr/local/sbin/serverws/setup/workdir.sh \
    && /usr/local/sbin/serverws/setup/ssh.sh

ENV WORKDIR_USER ${WORKDIR_USER}
ENV WORKDIR_GROUP ${WORKDIR_GROUP}
ENV WORKDIR_PATH ${WORKDIR_PATH}

ENTRYPOINT ["/usr/local/sbin/serverws/entrypoint.sh"]

#MASTER
FROM builder AS master 

RUN apt-get update && apt-get install -y --no-install-recommends \
        isc-dhcp-server \
    	ansible \
    && apt-get -qy autoremove \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

COPY ./container/sbin/master /usr/local/sbin/serverws/container

RUN chmod +x -R /usr/local/sbin/serverws/container \
    && /usr/local/sbin/serverws/container/dhcp-server.sh 

#NODE
FROM builder AS node

COPY ./container/sbin/node /usr/local/sbin/serverws/container

RUN chmod +x -R /usr/local/sbin/serverws/container \
    && /usr/local/sbin/serverws/container/dhcp-client.sh 