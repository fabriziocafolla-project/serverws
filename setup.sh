#!/bin/bash

set -eE -o functrace

failure() {
  local lineno=$1
  local msg=$2
  echo "Failed at $lineno: $msg"
}
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

set -o pipefail

phase(){
        local msg=${1:-""}
        echo "[FASE] ${msg}"
}

success(){
        local msg=${1:-""}
        echo "[SUCESS] ${msg}"
}

err(){
        local msg=${1:-"generico"}
        echo "[ERROR] ${msg}"
        exit 1
}

FILE_ENV="./.env"

DOCKERFILE_PATH="."
CONTAINER_PATH="container"
VOLUMES_PATH="${CONTAINER_PATH}/data"
VOLUMES_SERVERWS_PATH="${VOLUMES_PATH}/serverws"
VOLUMES_GITLAB_PATH="${VOLUMES_PATH}/gitlab"
VOLUMES_GITLAB_RUNNER_PATH="${VOLUMES_PATH}/gitlab-runner"
DEFAULT_USER="www-data"
DEFAULT_GROUP="www-data"


SUBNET_NODES="172.1.0.0/16"
SUBNET_GITLAB="172.2.0.0/16"
ALIAS_MASTER="master.private"
ALIAS_NODEA="nodea.private"
ALIAS_NODEB="nodeb.private"
ALIAS_GITLAB="gitlab.local"

parser(){
    options=$(getopt -l "help,default-user:,default-group:,workdir-path:,ssh,install-utils" -o "h u: g: w: s i" -a -- "$@")
    eval set -- "$options"

    while true
       do
        case $1 in
            -h|--help)
                showHelp
                ;;
            -u|--default-user)
                DEFAULT_USER=${2:-${DEFAULT_USER}}
                ;;
            -g|--default-group)
                DEFAULT_GROUP=${2:-${DEFAULT_GROUP}}
                ;;
            -w|--workdir-path)
                CONTAINER_PATH=${2:-${CONTAINER}}
                ;;
            -s|--ssh)
                SSHKEY=true
                ;;
            -i|--install-utils) 
              INSTALL_UTIL=true
                ;;
            --)
                shift
                break;;
        esac
        shift
    done

    shift "$(($OPTIND -1))"
}

_sshkey(){
  local _user=$1

  local _rsa=${VOLUMES_SERVERWS_PATH}/home/root/.ssh

  if [ ! -d ${_rsa} ] ; then
    mkdir -p ${_rsa}
  fi

  _rsa=${_rsa}/id_rsa
  
  sudo -u $_user ssh-keygen -q -t rsa -N '' -f $_rsa 2>/dev/null <<< y >/dev/null
}

update_env() {
  local _file=${1}
  local _key=${2}
  local _value=${3}
  echo "${_key}=${_value}" >> ${_file}
}

set_env() {
  phase "SET ENV"

  touch $FILE_ENV

  update_env $FILE_ENV "WORKDIR_USER" ${DEFAULT_USER}
  update_env $FILE_ENV "WORKDIR_GROUP" ${DEFAULT_GROUP}
  update_env $FILE_ENV "DOCKERFILE_PATH" ${DOCKERFILE_PATH}
  update_env $FILE_ENV "CONTAINER_PATH" ${CONTAINER_PATH}
  update_env $FILE_ENV "VOLUMES_PATH" ${VOLUMES_PATH}
  update_env $FILE_ENV "VOLUMES_SERVERWS_PATH" ${VOLUMES_SERVERWS_PATH}
  update_env $FILE_ENV "VOLUMES_GITLAB_PATH" ${VOLUMES_GITLAB_PATH}
  update_env $FILE_ENV "VOLUMES_GITLAB_RUNNER_PATH" ${VOLUMES_GITLAB_RUNNER_PATH}

  update_env $FILE_ENV "SUBNET_NODES" ${SUBNET_NODES}
  update_env $FILE_ENV "SUBNET_GITLAB" ${SUBNET_GITLAB}

  update_env $FILE_ENV "ALIAS_MASTER" ${ALIAS_MASTER}
  update_env $FILE_ENV "ALIAS_NODEA" ${ALIAS_NODEA}
  update_env $FILE_ENV "ALIAS_NODEB" ${ALIAS_NODEB}
  update_env $FILE_ENV "ALIAS_GITLAB" ${ALIAS_GITLAB}
}


# Crea il gruppo indicato nella default.conf
# Crea e aggiunge l'utente indicato nella default.conf al gruppo
# Crea la cartella di lavoro indicata nella .conf e assegna le facl
set_workdir(){
  phase "SET WORKDIR"

  if [ ! -d ${VOLUMES_SERVERWS_PATH} ] ; then
    sudo mkdir -p ${VOLUMES_SERVERWS_PATH}/{data,home,logs}
  fi

  if [ ! -d ${VOLUMES_GITLAB_PATH} ] ; then
    sudo mkdir -p ${VOLUMES_GITLAB_PATH}/{data,config,logs}
  fi

  if [ ! -d ${VOLUMES_GITLAB_RUNNER_PATH} ] ; then
    sudo mkdir -p ${VOLUMES_GITLAB_RUNNER_PATH}
  fi

  if [ "$(cut -d: -f1 /etc/group | sort | grep -x ${DEFAULT_GROUP})" == "" ] ;  then
    sudo groupadd ${DEFAULT_GROUP}
  fi

  if [ "$(cut -d: -f1 /etc/passwd | sort | grep -x ${DEFAULT_USER})" == "" ] ;  then
    sudo useradd -N -m -b "$(pwd)/${VOLUMES_SERVERWS_PATH}/home" -s /bin/bash ${DEFAULT_USER}
    sudo usermod -a -G $DEFAULT_GROUP $DEFAULT_USER
  fi

  # Set permessi 
  sudo chown -R ${DEFAULT_USER}:${DEFAULT_USER} ${VOLUMES_PATH}
  sudo chmod -R 774 ${VOLUMES_PATH}

  sudo setfacl -R -m u:${DEFAULT_USER}:rwx ${VOLUMES_PATH}
  sudo setfacl -R -d -m u:${DEFAULT_USER}:rwx ${VOLUMES_PATH}

  sudo setfacl -R -m g:${DEFAULT_GROUP}:rwx ${VOLUMES_PATH}
  sudo setfacl -R -d -m g:${DEFAULT_GROUP}:rwx ${VOLUMES_PATH}

  sudo setfacl -R -m o::r-X ${VOLUMES_PATH}
  sudo setfacl -R -d -m o::r-X ${VOLUMES_PATH}

  _sshkey $DEFAULT_USER
}

install_docker() {
  if [ "$(docker -v)" ] ; then
    echo "Docker already installed" && return
  fi

  sudo apt-get update

  sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

  sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

  sudo apt-get update
  sudo apt-get install docker-ce docker-ce-cli containerd.io

  sudo usermod -a -G docker ${DEFAULT_USER}


  # install docker-compose
  sudo curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose

  sudo newgrp docker
}

main() {
  if [ -f "$FILE_ENV" ] ; then
    echo "[ERROR] file .env exist"
    exit 1
  fi

  parser $@

  if [ ! -z $INSTALL_UTIL ] ; then
    sudo apt-get update
    sudo apt-get upgrade -y
    sudo apt-get install vim git wget make build-essential
    install_docker
  fi

  set_env

  set_workdir

  if [ ! -z ${SSHKEY} ]; then
    _sshkey $DEFAULT_USER
  fi

  exit 0
}

main $@
