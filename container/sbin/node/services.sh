#!/bin/bash

source /usr/local/sbin/serverws/base.sh

main() {
    #DHCP CLIENT
    ip addr flush dev eth0
    dhclient

    #SSH
    service ssh start
}

main $@
