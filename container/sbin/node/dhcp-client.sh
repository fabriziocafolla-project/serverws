#!/bin/bash

source /usr/local/sbin/serverws/base.sh

main() {
   cat << EOF > /etc/network/interfaces
auto  eth0
iface eth0 inet dhcp
EOF
}

main $@
