#!/bin/bash

source /usr/local/sbin/serverws/base.sh

main() {
    #DHCP SERVER
    ufw reload
    dhcpd

    #SSH
    service ssh start
}

main $@
