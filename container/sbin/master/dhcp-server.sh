#!/bin/bash

source /usr/local/sbin/serverws/base.sh

main() {
    ufw allow  67/udp

    touch /var/lib/dhcp/dhcpd.leases
    
    sed 's/INTERFACESv4=""/INTERFACESv4="eth1"/g' /etc/default/isc-dhcp-server
    sed 's/IIPV6=yes/IPV6=no/g' /etc/default/ufw

    cat << EOF > /etc/dhcp/dhcpd.conf
option domain-name "${ALIAS_MASTER:-"none.local"}";
option domain-name-servers ns1.${ALIAS_MASTER:-"none.local"};

default-lease-time 600;
max-lease-time 7200;
authoritative;
ddns-update-style none;

subnet 172.1.0.0 netmask 255.255.255.0 {
        option routers                  172.1.0.1;
        option subnet-mask              255.255.255.0;
        option domain-search            "${ALIAS_MASTER:-"none.local"}";
        option domain-name-servers      172.1.0.1;
        range   172.1.0.2   172.1.0.10;
}
EOF
}

main $@
