#!/bin/bash

source /usr/local/sbin/serverws/base.sh

main() {
  if [ ! -d "/root/.ssh" ] ; then
   mkdir -p /root/.ssh 
  fi

  if [ ! -f "/root/.ssh/authorized_keys" ] ; then
   touch /root/.ssh/authorized_keys 
   chmod 600 /root/.ssh/authorized_keys
  fi

  chown -R root:root /root/.ssh

  #Permette di fare il login come root
  sed -ri "s/^#PermitRootLogin .*$/PermitRootLogin yes/g" /etc/ssh/sshd_config
  #Non chiede conferma per aggiungere l'host ai know_hosts
  sed -ri "s/^#[[:space:]]*StrictHostKeyChecking .*$/StrictHostKeyChecking no/g;" /etc/ssh/ssh_config
}

main $@
