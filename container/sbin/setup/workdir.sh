#!/bin/bash

source /usr/local/sbin/serverws/base.sh

main(){
  if [ -z ${WORKDIR_USER} ] || [ -z ${WORKDIR_GROUP} ] || [ -z ${WORKDIR_PATH} ] ; then
    echo "Setup users fails, vars not found."
  fi

  #set root password
  echo "root:root" | chpasswd

  if [ "$(cut -d: -f1 /etc/passwd | sort | grep -x ${WORKDIR_USER})" == "" ] ;  then
    useradd -g $WORKDIR_GROUP -d $WORKDIR_PATH -s /bin/bash $WORKDIR_USER
  fi
  echo "$WORKDIR_USER:serverws" | chpasswd

  mkdir -p ${WORKDIR_PATH}

  chown -R $WORKDIR_USER:$WORKDIR_GROUP ${WORKDIR_PATH}
  chmod -R 774 ${WORKDIR_PATH}
  
  setfacl -R -m u:$WORKDIR_USER:rwx ${WORKDIR_PATH}
  setfacl -R -d -m u:$WORKDIR_USER:rwx ${WORKDIR_PATH}
  
  setfacl -R -m g:$WORKDIR_GROUP:rwx ${WORKDIR_PATH}
  setfacl -R -d -m g:$WORKDIR_GROUP:rwx ${WORKDIR_PATH}
  
  setfacl -R -m o::r-X ${WORKDIR_PATH}
  setfacl -R -d -m o::r-X ${WORKDIR_PATH}
}

main $@

